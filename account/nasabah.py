import pymysql.cursors
from module.conn import Connection
from module.password_encrypter import Password
from account.norek import NomorRekening
from account.saldo import Saldo
from random import randint

class Nasabah:
    def __init__(self):
        con = Connection()
        self.connection = con.get()

    def isExists(self, username):
        cursor = self.connection.cursor()

        #Execute
        cursor.execute('SELECT * FROM nasabah WHERE username = %s', username)
        data = cursor.fetchone()
        
        if data == None:
            return False
        else:
            return True

    def getLastNorekCreated(self):
        cursor = self.connection.cursor()

        #Execute
        cursor.execute('SELECT * FROM nasabah ORDER BY id_user DESC limit 1')
        data = cursor.fetchone()

        return data['norek']

    def generateNumber(self):
        return randint(100000000000000, 999999999999999)

    def Create(self, username, password, email, phone, address, firstname, lastname):
        #Check if username is not exists
        if not self.isExists(username):
            rekening = NomorRekening()
            norek = self.generateNumber()

            if not rekening.isExists(norek):
                cursor = self.connection.cursor()

                if len(password) <= 16:
                    pw_object = Password(password, '1129874678923456')
                    enc_password = pw_object.Aesis()
                    
                    #Execute
                    cursor.execute('INSERT INTO nasabah (username, password, email, phone, address, firstname, lastname, norek) values(%s, %s, %s, %s, %s, %s, %s, %s)', (username, enc_password, email, phone, address, firstname, lastname, str(norek)))
                    self.connection.commit()

                    #Create blank saldo 
                    saldo = Saldo()
                    norek = self.getLastNorekCreated()
                    saldo.CreateInitialize(norek)

                    return 'Success', 'Successfully create nasabah account', norek
                else:
                    return 'Failed', 'Password must less than 16 character', ''

        else:
            return 'Failed', 'Nasabah account is already exists', ''


    def Login(self, username, password):
        if self.isExists(username):
            cursor = self.connection.cursor()

            pw_object = Password(password, '1129874678923456')
            enc_password = pw_object.Aesis()

            #Execute
            cursor.execute('SELECT * FROM nasabah WHERE username = %s AND password = %s', (username, enc_password))
            data = cursor.fetchone()

            if data != None:
                return 'Success', 'Successfully login'
            else:
                return 'Failed', 'Username or password is wrong'
        else:
            return 'Failed', 'Nasabah account is not registered'