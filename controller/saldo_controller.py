from account.saldo import Saldo
from module.keys import Keys

class Saldo_Controller:

    def __init__(self, norek, apikey):
        self.norek = norek
        self.apikey = apikey

    def Check(self):
        saldo = Saldo()

        keys = Keys()
        data = []
        if keys.isExists(self.apikey):
            status, messages, data = saldo.Check(self.norek)
        else:
            status = 'Failed'
            messages = 'Apikey is invalid'
        
        profile_list = [
            data
        ]

        profile_data = {
            'status_info': status,
            'status_messages': messages,
            'profile_data': profile_list
        }

        return profile_data

    def CheckAll(self):
        saldo = Saldo()

        keys = Keys()
        data = []
        if keys.isExists(self.apikey):
            status, messages, data = saldo.CheckAll()
        else:
            status = 'Failed'
            messages = 'Apikey is invalid'
        
        profile_list = [
            data
        ]

        profile_data = {
            'status_info': status,
            'status_messages': messages,
            'profile_data': profile_list
        }

        return profile_data